Gem::Specification.new do |spec|
  spec.name          = "jekyll-twitch"
  spec.version       = '1.0.0'
  spec.authors       = ["Phosphorus"]
  spec.email         = ["lic.fernando.pastorelli@gmail.com"]

  spec.summary       = %q{jekyll plugin to generate html snippets for embedding Twitch videos/Streams/clips}
  spec.description   = %q{jekyll plugin to generate html snippets for embedding Twitch videos/Streams/clips}
  spec.homepage      = "https://gitlab.com/phosphorus-moscu/jekyll-twitch"
  spec.license       = 'MIT'

  spec.files       = ["jekyll-twitch.rb"]

  spec.add_dependency 'jekyll'
  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
end
