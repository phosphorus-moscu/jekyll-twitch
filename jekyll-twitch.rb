module Jekyll
  class TwitchEmbed < Liquid::Tag

    def initialize(tagName, content, tokens)
      super
      @content = content
    end

    def render(context)
      twitch_url = "#{context[@content.strip]}"
      if twitch_url[/clips\.twitch\.tv\/([^\?]*)/]
        typecase = "clip"
        @twitch_id = $1
      elsif twitch_url[/twitch\.tv\/videos\/([0-9A-Za-z-_]{9})([^\&\?]*).*/]
        twitch_url[/twitch\.tv\/videos\/([0-9A-Za-z-_]{9})(\?collection?=?([^\&\?]*).*|)/]
        @twitch_id = $1
        typecase = "video"
      else
        twitch_url[/twitch\.tv\/([^\?]*)/]
        @twitch_id = $1
        typecase = "stream"
      end

      tmpl_path = File.join Dir.pwd, "_includes", "twitch.html"
      if File.exist?(tmpl_path)
        tmpl = File.read tmpl_path
        site = context.registers[:site]
        tmpl = (Liquid::Template.parse tmpl).render site.site_payload.merge!({"twitch_id" => @twitch_id})
      else
        case typecase
        when "stream"
          %Q{<iframe src="https://player.twitch.tv/?channel=#{ @twitch_id }" frameborder="0" scrolling="no" height="500" width="350"></iframe> #{typecase}}
        when "video"
          %Q{<iframe src="https://player.twitch.tv/?autoplay=false&video=v#{ @twitch_id }" frameborder="0" allowfullscreen="true" scrolling="no" height="378" width="620"></iframe> #{typecase}}
        when "clip"
          %Q{<iframe src="https://clips.twitch.tv/embed?clip=#{ @twitch_id }" frameborder="0" allowfullscreen="true" height="378" width="620"></iframe>#{typecase}}
        else
          %Q{<p>Video no disponible :(</p>}
        end
      end
    end
  end
end
Liquid::Template.register_tag('twitch', Jekyll::TwitchEmbed)
